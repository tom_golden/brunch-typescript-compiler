"use strict";

var ts = require("typescript");

function BrunchTypescriptCompiler(config) {

}

BrunchTypescriptCompiler.prototype.brunchPlugin = true;
BrunchTypescriptCompiler.prototype.type = "javascript";
BrunchTypescriptCompiler.prototype.extension = "ts";

BrunchTypescriptCompiler.prototype.compile = function(params) {
    var compiled = ts.transpileModule(params.data, {});//TODO Use config from brunch file, along with tsconfig

    return Promise.resolve({
        data: compiled.outputText,
        map: compiled.sourceMapText
    });
}

module.exports = BrunchTypescriptCompiler;